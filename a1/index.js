console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	let getDetails = function () {
		let fullName = prompt("What is your name?");
		let age = prompt("How old are you?");
		let location = prompt("Where do you live?");
		alert("Thank you for your input!");

		console.log("Hello, " + fullName + ".");
		console.log("You are " + age + " years old.");
		console.log("You live in " + location + ".");
	};

	getDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	let displayMusic = function () {
		console.log("1. Parokya ni Edgar");
		console.log("2. Eraserheads");
		console.log("3. Ben & Ben");
		console.log("4. Panic! at the Disco");
		console.log("5. Fall Out Boy");
	};

	displayMusic();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	let printMovies = function () {
		let movies = ["Saving Private Ryan", "Hacksaw Ridge", "Kimi no Na Wa", "Interstellar", "Inception"];
		let ratings = [94, 84, 98, 72, 87];

		console.log("1. " + movies[0]);
		console.log("Rotten Tomatoes Rating: " + ratings[0] + "%");
		console.log("2. " + movies[1]);
		console.log("Rotten Tomatoes Rating: " + ratings[1] + "%");
		console.log("3. " + movies[2]);
		console.log("Rotten Tomatoes Rating: " + ratings[2] + "%");
		console.log("4. " + movies[3]);
		console.log("Rotten Tomatoes Rating: " + ratings[3] + "%");
		console.log("5. " + movies[4]);
		console.log("Rotten Tomatoes Rating: " + ratings[4] + "%");
	};

	printMovies();
	

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();
let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

//console.log(friend1);
//console.log(friend2);